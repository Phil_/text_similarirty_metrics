import pandas as pd
import numpy as np
import pytest
from matplotlib import pyplot as plt

from textmetrics.metric_report import calculate_metrics, metrics_barplot
from textmetrics.metrics import Corpus, MetricReturn, Metric


def test_calculate_metrics():
    def metric_1(c1: Corpus, c2: Corpus) -> MetricReturn:
        out = {"metric_1": {"score": 1.0}}
        return out

    def metric_2(c1: Corpus, c2: Corpus) -> MetricReturn:
        out = {
            "metric_2a": {"score": 2.0, "some_other_val": 0.1},
            "metric_2b": {"score": 3.0},
        }
        return out

    def metric_3(c1: Corpus, c2: Corpus) -> MetricReturn:
        out = {
            "metric_3": {"score": 4.0},
        }
        return out

    model_to_corpora = {
        "model_1": {
            "corpus1": (
                [["This", "is", "a", "mock"]],
                [["This", "is", "another", "mock"]],
            ),
            "corpus2": (
                [["This", "is", "a", "mock"]],
                [["This", "is", "another", "mock"]],
            ),
        },
        "model_2": {
            "corpus1": (
                [["This", "is", "a", "mock"]],
                [["This", "is", "yet", "another", "mock"]],
            ),
            "corpus2": (
                [["This", "is", "a", "mock"]],
                [["This", "is", "yet", "another", "mock"]],
            ),
        },
    }

    actual = calculate_metrics(
        model_to_corpora=model_to_corpora, metrics=[metric_1, metric_2, metric_3]
    )

    expected = get_metric_df()

    pd.testing.assert_frame_equal(actual, expected)


def get_metric_df():
    """
    Generate a synthetic metric dataframa for testing purposes
    Returns:

    """
    columns_dict = {
        "model": ["model_1"] * 8 + ["model_2"] * 8,
        "corpus": ["corpus1"] * 4 + ["corpus2"] * 4 + ["corpus1"] * 4 + ["corpus2"] * 4,
        "metric": ["metric_1", "metric_2a", "metric_2b", "metric_3"] * 4,
        "score": [1.0, 2.0, 3.0, 4.0] * 4,
    }
    expected = pd.DataFrame.from_dict(columns_dict, orient="columns")
    return expected


def get_metric_df_2(
    n_models: int, n_corpora: int, n_metrics: int
):
    """
    A similar function as get_metric_df but now with parameters
    This function also generates a synthetic metric dataframe for testing purposes
    Args:
        n_models ():
        n_corpora ():
        n_metrics ():

    Returns:

    """
    model = []
    for i in range(n_models):
        model += [f"model_{i}" for j in range(n_corpora * n_metrics)]
    corpus = []
    for i in range(n_models):
        for j in range(n_corpora):
            corpus += [f"corpus_{j}" for k in range(n_metrics)]
    metric = []
    for i in range(n_models * n_corpora):
        metric += [f"metric_{j}" for j in range(n_metrics)]
    score = []
    for i in range(n_models * n_corpora):

        score += [
            np.abs(np.random.normal(j,1))
            for j in range(n_metrics)
        ]

    columns_dict = {
        "model": model,
        "corpus": corpus,
        "metric": metric,
        "score": score,
    }
    expected = pd.DataFrame.from_dict(columns_dict, orient="columns")
    return expected

@pytest.mark.skip
def test_metrics_boxplot():
    """
    This function makes a plot of a metric dataframe.
    I've set it to pytest ignore because this is not an automated test
    but a manual test meaning: start this function and see if the plot looks good.
    Returns:

    """
    metric_df2 = get_metric_df_2(
        n_models=3, n_corpora=3, n_metrics=4
    )

    fig = metrics_barplot(metric_df2)

    plt.show()
