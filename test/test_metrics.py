from typing import Iterable

import numpy as np
from numpy import testing as np_testing
from textmetrics.corpus import BoWCorpus
import gensim
from textmetrics.metrics import (
    lsi_similarity,
    bleu,
    gleu,
    rouge,
    _ReferencesWrapperCorpus,
    _TokensToStringSeparatedByWhitespaceWrapperCorpus,
)


class SimpleTokenizedCorpus:
    """
    This  is a very simple Corpus that treats each line as a document
    and tokenizes it using gensim's simple_preprocess
    """

    def __init__(self, filename: str = "test_corpus.txt"):
        self.filename = filename

    def __iter__(self):
        with open(self.filename, "r") as file:
            for line in file:
                yield gensim.utils.simple_preprocess(line)


def test__ReferencesWrapperCorpus():
    # test if the corpus does its job of putting documents into a list
    corpus = SimpleTokenizedCorpus()
    wrapped_corpus = _ReferencesWrapperCorpus(corpus)
    for i, j in zip(corpus, wrapped_corpus):
        assert [i] == j


def test__TokensToStringSeperatedByWhitespaceWrapperCorpus():
    # test if the corpus does its job of appending the tokens together
    # into a string of tokens separated by whitespace

    def into_string(tokens: Iterable[str]) -> str:
        return " ".join(tokens)

    assert "this is a string" == into_string(["this", "is", "a", "string"])

    corpus = SimpleTokenizedCorpus()
    wrapped_corpus = _TokensToStringSeparatedByWhitespaceWrapperCorpus(corpus)
    for i, j in zip(corpus, wrapped_corpus):
        assert into_string(i) == j


def test_lsi_similarity_same_documents():
    # if two exact documents are passed the cosine similarity should be 1
    corpus_1 = SimpleTokenizedCorpus()
    corpus_2 = SimpleTokenizedCorpus()
    metric = lsi_similarity(corpus_1, corpus_2, random_seed=17)["lsi_similarity"]["score"]
    np_testing.assert_almost_equal(metric, 1.0)


def test_lsi_different_documents():
    # if two completely documents are passed the cosine similarity should be 0 if both texts have the same number of documents
    corpus_1 = SimpleTokenizedCorpus("test_corpus.txt")
    corpus_2 = SimpleTokenizedCorpus("test_corpus2.txt")

    metric = lsi_similarity(corpus_1, corpus_2, random_seed=17)["lsi_similarity"]["score"]

    np_testing.assert_almost_equal(metric, 0.0)


def test_bleu_same_document():
    # if two exact documents are passed the score should be 1
    corpus_1 = SimpleTokenizedCorpus("test_corpus.txt")
    corpus_2 = SimpleTokenizedCorpus("test_corpus.txt")
    bleu_score = bleu(corpus_1, corpus_2)["bleu"]["score"]
    np_testing.assert_almost_equal(bleu_score, 1.0)


def test_bleu_different_document():
    # if two different documents are passed the score should be 0
    corpus_1 = SimpleTokenizedCorpus("test_corpus.txt")
    corpus_2 = SimpleTokenizedCorpus("test_corpus2.txt")
    bleu_score = bleu(corpus_1, corpus_2)["bleu"]["score"]
    np_testing.assert_almost_equal(bleu_score, 0.0)


def test_gleu_same_document():
    # if two exact documents are passed the score should be 1
    corpus_1 = SimpleTokenizedCorpus("test_corpus.txt")
    corpus_2 = SimpleTokenizedCorpus("test_corpus.txt")
    gleu_score = gleu(corpus_1, corpus_2)["gleu"]["score"]
    np_testing.assert_almost_equal(gleu_score, 1.0)


def test_gleu_different_document():
    # if two exact documents are passed the score should be 0
    corpus_1 = SimpleTokenizedCorpus("test_corpus.txt")
    corpus_2 = SimpleTokenizedCorpus("test_corpus2.txt")
    gleu_score = gleu(corpus_1, corpus_2)["gleu"]["score"]
    np_testing.assert_almost_equal(gleu_score, 0.0)


def test_rouge_same_document():
    # if two exact documents are passed the score should be 1
    corpus_1 = SimpleTokenizedCorpus("test_corpus.txt")
    corpus_2 = SimpleTokenizedCorpus("test_corpus.txt")
    rouge_scores = rouge(corpus_1, corpus_2)
    for rouge_name, scores in rouge_scores.items():
        for score_name, score in scores.items():
            np_testing.assert_almost_equal(score, 1)


def test_rouge_differnt_document():
    # if two compleaty different documents are passed the score should be 0
    corpus_1 = SimpleTokenizedCorpus("test_corpus.txt")
    corpus_2 = SimpleTokenizedCorpus("test_corpus2.txt")
    rouge_scores = rouge(corpus_1, corpus_2)
    for rouge_name, scores in rouge_scores.items():
        for score_name, score in scores.items():
            np_testing.assert_almost_equal(score, 0)
