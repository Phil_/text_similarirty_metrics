from textmetrics.corpus import (
    BoWCorpus,
    AppendCorpus,
    BySentenceCorpus,
    ByParagraphCorpus,
)
import gensim
from nltk.tokenize import sent_tokenize, word_tokenize


class SimpleTokenizedCorpus:
    """
    This  is a very simple Corpus that treats each line as a document
    and tokenizes it using gensim's simple_preprocess
    """

    def __iter__(self):
        with open("test_corpus.txt", "r") as file:
            for line in file:
                yield gensim.utils.simple_preprocess(line)


def test_BoWBCorpus():
    """
    This tests if the BoW Corpus is correctly implemented based on an example
    where each line is a document and the tokenizer is gensims simple_preprocess
    Returns:

    """
    s = SimpleTokenizedCorpus()
    c = BoWCorpus(s)
    expected = [
        [
            (0, 1),
            (1, 1),
            (2, 1),
        ],
        [
            (0, 1),
            (2, 1),
            (3, 1),
        ],
        [
            (0, 1),
            (1, 1),
            (2, 1),
        ],
        [(0, 1), (1, 1), (2, 1), (3, 3)],
    ]

    assert len(list(c)) != 0
    for expected_vec, actual in zip(expected, c):
        assert actual == expected_vec
    # test if one can iterate over it multiple times
    for expected_vec, actual in zip(expected, c):
        assert actual == expected_vec


def test_BowCorpus_corpus_to_bow():
    """
    This tests if the BoWCorpus can transform a corpus into a bow representation
    Returns:

    """
    s = SimpleTokenizedCorpus()
    c = BoWCorpus(s)
    transformed = c.corpus_to_bow(s)
    for actual, expected in zip(transformed, c):
        assert actual == expected


def test_BowCorpus_document_to_bow():
    s = SimpleTokenizedCorpus()
    c = BoWCorpus(s)
    doc = gensim.utils.simple_preprocess("This is a text")
    doc2 = gensim.utils.simple_preprocess("This is my text")

    transformed_1 = c.document_to_bow(doc)
    transformed_2 = c.document_to_bow(doc2)

    expected_1 = [(0, 1), (2, 1), (3, 1)]
    expected_2 = [(0, 1), (2, 1), (3, 1)]
    assert transformed_1 == expected_1
    assert transformed_2 == expected_2


def test_AppendCorpus():
    """
    This tests if the AppendCorpus is able to just combine two Iterables together
    Returns:

    """
    with open("test_corpus.txt") as corpus_file:
        c1 = corpus_file.readlines()

    class CorpusWithRessource:
        def __init__(self):
            self.file = "test_corpus.txt"

        def __iter__(self):
            with open(self.file, "r") as f:
                for line in f:
                    yield line

    c2 = CorpusWithRessource()
    expected = c1 + c1
    append = AppendCorpus([c1, c2])
    for expected_doc, actual in zip(expected, append):
        assert actual == expected_doc

    for expected_doc, actual in zip(expected, append):
        assert actual == expected_doc


def test_is_a_corpus():
    corpus = SimpleTokenizedCorpus()
    also_a_corpus = (["those", "are", "tokens"], ["so", "many", "tokens"])
    also_a_corpus_2 = (("those", "are", "tokens"), ("so", "many", "tokens"))
    not_a_corpus_1 = ["those", "are", "tokens"]
    not_a_corpus_2 = "Tose are tokens too"
    not_a_corpus_3 = [
        [(1, 1), (2, 2)],
        [(0, 1), (1, 1)],
    ]  # a BoW Corpus is not a corpus as a corpus of tokens
    sadly_a_corpus = (["those", "are", "tokens"], ["so", "many", "tokens"], object())

    assert gensim.utils.is_corpus(corpus)[0]
    assert gensim.utils.is_corpus(also_a_corpus)[0]
    assert gensim.utils.is_corpus(also_a_corpus_2)[0]
    assert not gensim.utils.is_corpus(not_a_corpus_1)[0]
    assert not gensim.utils.is_corpus(not_a_corpus_2)[0]
    assert not gensim.utils.is_corpus(not_a_corpus_3)[0]
    assert gensim.utils.is_corpus(sadly_a_corpus)[0]


def test_ByParagraphCorpus():
    actual_corpus = ByParagraphCorpus(
        "test_paragraph_corpus.txt", tokenizer=word_tokenize, min_sentences=2
    )
    expected = (
        """
    This is a paragraph.
    It contains 3 sentences.
    Yes three!
    """,
        """
        Some Paragraphs are long.
Because they have a lot of sentences. With multiple sentences in a single line. How crazy is that?
Some Paragraphs are long.
Because they have a lot of sentences. With multiple sentences in a single line. How crazy is that?
Some Paragraphs are long.
Because they have a lot of sentences. With multiple sentences in a single line. How crazy is that?
    """,
        """
    Some are tricky and have abbrevietions Like Mr. Sr. Sr. Jr.
    Some are tricky and have abbrevietions Like Mr. Sr. Sr. Jr.
    """,
    )
    expected = (word_tokenize(s) for s in expected)
    i = 0
    for a_paragraph, e_paragraph in zip(actual_corpus, expected):
        assert a_paragraph == e_paragraph
        i += 1
    assert i == 2


def test_BySentenceCorpus():
    actual_corpus = BySentenceCorpus("test_paragraph_corpus.txt",tokenizer=word_tokenize, language="english")
    expected_corpus = \
    """
    This is a paragraph.
    It contains 3 sentences.
    Yes three!
    Some Paragraphs are short.
    Some Paragraphs are long.
    Because they have a lot of sentences.
    With multiple sentences in a single line.
    How crazy is that?
    Some Paragraphs are long.
    Because they have a lot of sentences.
    With multiple sentences in a single line.
    How crazy is that?
    Some Paragraphs are long.
    Because they have a lot of sentences.
    With multiple sentences in a single line.
    How crazy is that?
    Some are tricky and have abbrevietions Like Mr. Sr. Sr. Jr.
    Some are tricky and have abbrevietions Like Mr. Sr. Sr. Jr.
    """.split("\n")[1:-1]
    expected_corpus = list(map(word_tokenize, expected_corpus))
    i = 0
    for a_paragraph, e_paragraph in zip(actual_corpus, expected_corpus):
        assert a_paragraph == e_paragraph
        i += 1
    assert i == 18