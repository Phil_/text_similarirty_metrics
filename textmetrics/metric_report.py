"""
This modules offers two main functions:
One for calculating metrics of different corpora and gathering the results into a dataframe
and turning the dataframe into a plot
"""

from typing import List, Dict, Iterable, Tuple
import pandas as pd
import seaborn as sns
from collections import defaultdict
from matplotlib import pyplot as plt
from typing import Callable

from textmetrics.metrics import (
    bleu,
    gleu,
    rouge,
    lsi_similarity,
    Metric,
    Corpus,
    MetricReturn,
)

from textmetrics.corpus import ByLineCorpus

# in this array all the know metrics are collected
known_metrics: Tuple[Metric, ...] = (bleu, gleu, rouge, lsi_similarity)


def generate_corpora_from_files(
    ref_corpus_path: str,
    prediction_corpus_path: str,
    tokenizer: Callable[[str], list[str]],
) -> Tuple[Corpus, Corpus]:
    """
    This function creates two corpora which can be used for calculating metrics.

    Args:
        ref_corpus_path (str): path to a textfile. It is assumed each line is a document.
        prediction_corpus_path (str): path to a textfile. It is assumed each line is a document.
        tokenizer (Callable[[str], list[str]]): a function that processes a string into a list of strings [tokens]

    Returns:
        Tuple[Iterable[List[str]], Iterable[List[str]]]: Two corpora which fit the definition of a corpus from metrics.py
        first is reference second is prediciton
    """
    return ByLineCorpus(ref_corpus_path, tokenizer), ByLineCorpus(
        prediction_corpus_path, tokenizer
    )


def calculate_metrics(
    model_to_corpora: Dict[str, Dict[str, Tuple[Corpus, Corpus]]],
    metrics: Iterable[Metric] = known_metrics,
) -> pd.DataFrame:
    """
    This functions calculates metrics for
    Args:
        model_to_corpora:
            (Dict that maps a model name to a dict that maps a corpus name
            to a reference and a generated corpus, corpus is defined in metrics.py):
            The first entry of the tuple is the candidate/prediction/translation. The second entry is the reference.

        metrics (Iterable of metrics defined in metrics.py):
            if no value is provided, the known metrics of this module are used
    Returns:
        pandas.Dataframe with columns model, corpus, metric, and score
        metric is the name of the model
        corpus is the name of the corpus
        metric is the name of the metric
        score is the achieved score using metric on the corpora created by model

    """

    # this will be transformed into a pandas dataframe
    # it contains mappings of metric names to list of metric values and maps model
    out = defaultdict(lambda: [])
    for modelname, corpora_dict in model_to_corpora.items():
        for corpus_name, corpora in corpora_dict.items():
            candidate_corpus, reference_corpus = corpora
            for metric in metrics:
                metric_result: MetricReturn = metric(candidate_corpus, reference_corpus)
                for metric_name, metric_val_dict in metric_result.items():
                    out["model"].append(modelname)
                    out["corpus"].append(corpus_name)
                    out["metric"].append(metric_name)
                    out["score"].append(metric_val_dict["score"])

    df = pd.DataFrame.from_dict(out, orient="columns")
    return df


def metrics_barplot(metric_dataframe: pd.DataFrame) -> plt.Figure:
    """
    This functions creates boxplots from the dataframes produces by calculate_metrics.
    Args:
        metric_dataframes (Dict[str, pd.DataFrame]): Mapping of model name to Dataframe of Scores
        of Metrics on different Corpora

    Returns:
        Dict[str, plt.Figure]: Mapping of model name to pyplot Figure containing a barplot

    """

    facet: sns.FacetGrid = sns.catplot(
        x="model",
        y="score",
        hue="metric",
        data=metric_dataframe,
        col="corpus",
        # col_wrap=len(metric_dataframe["corpus"].unique()),
        kind="bar",
    )
    facet.tight_layout()
    return facet.figure
