import numpy as np
from typing import Union, Callable
from textmetrics.corpus import BySentenceCorpus


def get_sentence_histogram(
    text: Union[BySentenceCorpus, str],
    tokenizer: Callable[[str], list[str]] = None,
    language: str = "english",
) -> np.array:
    """
    This function calculates a histogram of sentence length of a text in a file
    Args:
        text(str or BySentenceCorpus): a path to a textfile or a BysentenceCorpus
        tokenizer (Callable[[str], list[str]]): a function that splits a text into tokens
        if text is a string this argument is needed
        if text is a BySentenceCorpus it is ignored
        language (str): language of nltks punkt model. either german or english
    Returns:
        a numpy.ndarray of integers with length n where n is the number of different sentence lengths
    """
    if isinstance(text, str):
        if tokenizer is None:
            raise ValueError("No tokenizer provided!")
        text_corpus = BySentenceCorpus(text, tokenizer, language)
    elif isinstance(text, BySentenceCorpus):
        text_corpus = text
    else:
        raise TypeError(
            f"text must be a string or a BySentenceCorpus but is {type(text)}"
        )

    lengths = list(map(len, text_corpus))
    bin_count = max(lengths)
    hist = np.bincount(lengths, minlength=bin_count)
    return hist
