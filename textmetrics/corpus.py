"""
This module contains different Corpora.
A corpus is an iterable of Documents.
A document in this library already tokenized meaning it's a list of strings.
"""

import re

import gensim
from typing import Callable, Tuple, Iterator, List, Iterable
from nltk.tokenize import sent_tokenize


class AppendCorpus:
    """
    This class appends any number of corpora into a single corpus.
    """

    def __init__(self, corpora: List[Iterable[List[str]]]):
        self.corpora = corpora

    def __iter__(self):
        for corpus in self.corpora:
            yield from corpus


class BoWCorpus:
    """
    This Corpus turns each tokenized document of the provided corpus into a bag of words vector

    Attributes:
        corpus (Iterable[List[str]]): an iterable of strings is a corpus
        dictionary (gensim.corpora.Dictionary): a dictionary of gensim needed for the Bag of Words
    """

    def __init__(self, corpus: Iterable[List[str]]):
        """

        Args:
            corpus (Iterable[List[str]): an iterable of tokenized documents

        """
        self.corpus = corpus
        self.dictionary = gensim.corpora.Dictionary(self.corpus)
        # self.dictionary.add_documents(self.corpus)

    def __iter__(self) -> Iterator[List[Tuple[int, int]]]:
        """
        Iterate over the corpus to receive a Bag or Words vector
        Returns:

        """
        yield from map(self.dictionary.doc2bow, self.corpus)

    def corpus_to_bow(
        self, corpus: Iterable[List[str]]
    ) -> Iterable[List[Tuple[int, int]]]:
        """
        Transform a corpus into a corpus of Bag of Words
        documents based on the tokens in this document
        Args:
            corpus (Iterable[List[str]]): a corpus of tokens

        Returns:
            Iterable[List[Tuple[int, int]]]: a corpus of bow documents
        """

        class WrappedCorpus:
            def __init__(inner_self, corpus: Iterable[List[str]]):
                inner_self.inner_corpus = corpus

            def __iter__(inner_self):
                yield from map(self.document_to_bow, inner_self.inner_corpus)

        return WrappedCorpus(corpus)

    def document_to_bow(self, document: List[str]) -> List[Tuple[int, int]]:
        """
        Transform a document into a Bag of Words document
        meaning a list of tuples of id and count
        Args:
            document (List[str]): a list of tokens

        Returns:
            List[Tuple[int, int]]: a list of tuples of id and count
        """
        return self.dictionary.doc2bow(
            document, allow_update=False, return_missing=False
        )


class BySentenceCorpus:
    """
    This class turns each sentence in a text into a document

    """

    def __init__(
        self,
        textpath: str,
        tokenizer: Callable[[str], list[str]],
        language: str = "english",
    ):
        """
        Args:
            textpath (str): path to a text file
            tokenizer (Callable[[str], list[str]]): function that turns a string into a list of tokens
            language (str): language of the Punkt model from nltk. Usually english or german. english is default
        """
        with open(textpath, "r") as file:
            text = file.read()

        self.sentences = sent_tokenize(text, language)
        self.tokenizer = tokenizer

    def __iter__(self) -> Iterator[list[str]]:
        """
        Returns:

        """
        yield from map(self.tokenizer, self.sentences)


class ByParagraphCorpus:
    """
    This class turns each paragraph in a text into a document.
    A paragraph is a sequenze of text ended by two newlines.

    e.g.
    This is a paragraph.

    This is also a paragraph.
    But it has two sentences


    Notes:
        This class can return paragraphs which contain only a single sentence.
    """

    def __init__(
        self,
        textpath: str,
        tokenizer: Callable[[str], list[str]],
        min_sentences: int = 1,
        language: str = "english",
    ):
        """
        Args:
            textpath (str): path to a textfile
            tokenizer (Callable[[str], list[str]]): function that turns a string into a list of tokens
            min_sentences (int): reject all paragraphs with less sentences that min_sentences
            language (str): language of the Punkt model from nltk. Usually english or german. english is default
        """
        self.paragraphs = []
        with open(textpath, "r") as file:
            # each line is ended by a
            paragraph = ""
            for line in file:
                line = line.strip()
                if line.strip() == "":
                    sentences = sent_tokenize(paragraph, language)
                    if len(sentences) > min_sentences:
                        self.paragraphs.append(" ".join(sentences).strip())
                    paragraph = ""
                else:
                    paragraph += " " + line

        self.tokenizer = tokenizer

    def __iter__(self) -> Iterator[list[str]]:
        """
        Returns:

        """
        yield from map(self.tokenizer, self.paragraphs)


class ByLineCorpus:
    """
    This class turns each line of a textfile into a document
    """

    def __init__(self, textfile_path: str, tokenizer: Callable[[str], list[str]]):
        """

        Args:
            textfile_path (str): path to a textfile
            tokenizer (Callable[[str], list[str]]): function that turns a string to a list of tokens
        """
        self.filepath = textfile_path
        self.tokenizer = tokenizer

    def __iter__(self) -> Iterator[list[str]]:
        with open(self.filepath, "r") as file:
            yield from map(self.tokenizer, file)
