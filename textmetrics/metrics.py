"""
This module contains functions which are metrics.

A metric is a function that takes two corpora and returns a dictionary of dictionaries.
The outer dictionary has the name of the calculated metrics as the keys, in case there are metrics
which compute multiple scores.
The inner dictionary has a key score which is the main score. And other keys which are other statistics
calculated by the metric.
"""

import gensim
import numpy as np
from typing import Iterable, Callable, List, Iterator, Tuple, Dict, Union
from textmetrics.corpus import BoWCorpus, AppendCorpus
from datasets import load_metric

# datasets is a library from Hugging Face Inc. which provides an interface
# to a bunch of public nlp datasets and nlp metrics

Corpus = Iterable[List[str]]
MetricReturn = Dict[str, Dict[str, float]]
Metric = Callable[[Corpus, Corpus, ...], MetricReturn]


class _ReferencesWrapperCorpus:
    """
    This class is an adapter for the datasets metrics.
    Many of them expect a list of lists of tokenized sentences for the references in the metrics
    But we only have a single reference. Therefor this Corpus wraps a single corpus to fit
    the expected input
    """

    def __init__(self, corpus: Corpus):
        """

        Args:
            corpus (Iterable[List[str]]): tokenized corpus of the reference text to be wrapped
        """
        self._corpus = corpus

    def __iter__(self) -> Iterator[List[List[str]]]:
        """

        Yields: A list containing a List of strings
        """
        yield from ([doc] for doc in self._corpus)


class _TokensToStringSeparatedByWhitespaceWrapperCorpus:
    """
    This class is an adapter for the datasets metrics.
    Some metrics expect documents to be a string with tokens separated by whitespaces.
    But a document in this library is defined as a list of strings.
    To this corpus exists to transform a list of tokens into a string of tokens separated by whitespace.
    """

    def __init__(self, corpus: Corpus):
        """

        Args:
            corpus (Iterable[List[str]]): tokenized corpus of the reference text to be wrapped
        """
        self._corpus = corpus

    def __iter__(self) -> Iterator[str]:
        """

        Yields: A string of tokens separated by whitespace.

        """
        yield from (" ".join(doc) for doc in self._corpus)


def rouge(
    candidate: Corpus,
    reference: Corpus,
    rouge_types: List[str] = ("rouge1", "rouge2", "rougeL"),
) -> MetricReturn:
    """
    Rouge is a set of metrics developed to evaluate machine summarization and machine translation.
    Rogue can by devided into Rouge-n and Rougle-L.
    Rouge-n is based on n-gram overlap.
    Rouge-L is based on longest common subsequence.

    In this library there are 4 different Rouge-Scores implemented:
    Rouge-1, Rouge-2, Rouge-L and Rouge-Lsum.

    Rouge-1 is calculated by counting the recall and precision of the unigrams that is:
    Recall = m_1 / l_{r,1}, Precision = m_1 / l_{c,1}
    where m_1 is the number of matching unigrams in the reference, l_{r,1} is the number of unigrams in the reference
    and l_{c,1} is the number of unigrams in the candidate.
    Finally a F1-Score is computed 2 * ( precision * recall / (precision + recall) )

    Similarly Rouge-2 is calculated as:
    Recall = m_2 / l_{r,2}, Precision = m_2 / l_{c,2}
    where l_{c,r} and l_{c,2} is the number of unigrams in the reference and candidate respectively.

    Rouge-1 and Rouge-2 tend to be worse for longer documents because there are more possibilities
    for different n-grams in longer documents. Therefor one should also consider Rouge-L and Rouge-Lsum.

    Rogue-L works by calculating Precision, Recall and F1 of the longest common subsequence(lcs). The longest common
    subsequence is a sequence that appears in the same relative order but the tokens do not have to be
    continuous. That means Rogue-L is calculated by:
    Recall = LCS / l_{r,1}, Precision = LCS / l_{c,1}

    # todo explain Rouge-Lsum

    Args:
        candidate (Iterable[List[str]]): a corpus of tokenized documents, this is usually machine generated
        reference (Iterable[List[str]]):  a corpus of tokenized documents, this is usually made by humans
        rouge_types: A list of rouge types to calculate.
        Valid names:
        `"rouge{n}"` (e.g. `"rouge1"`, `"rouge2"`) where: {n} is the n-gram based scoring,
        `"rougeL"`: Longest common subsequence based scoring.
        `"rougeLSum"`: rougeLsum splits text using `"
        default is [`"rouge1"`, `"rouge2"`, `"rougeL"`]
    Returns:
        Dictionary that maps types of Rogue Scores to Tuples of precision, recall and f1
    """

    pred = _TokensToStringSeparatedByWhitespaceWrapperCorpus(candidate)
    ref = _TokensToStringSeparatedByWhitespaceWrapperCorpus(reference)
    _rouge = load_metric("rouge")
    _rouge.add_batch(predictions=pred, references=ref)
    result = _rouge.compute(rouge_types=rouge_types)
    # rouge bootstraps the score and generated confidence intervals from them an alpha=0.05
    # those can be found as keys of result under the attributes low, mid, and high
    # low is the 2.5% quantile, mid is the mean and high is the 97.5% quantile
    # for simplicity we will just use mid
    out = dict()
    for k in result.keys():
        out[k] = {
            "precision": result[k].mid.precision,
            "recall": result[k].mid.recall,
            "score": result[k].mid.fmeasure,
            "f1": result[k].mid.fmeasure,
        }
    return out


def bleu(candidate: Corpus, reference: Corpus) -> MetricReturn:
    """
    Compute the Bleu Score between 2 corpora.
    Bleu is usually used as a simple method of comparing a machine translation to a gold standard which
    is usually created by humans.
    Bleu works by calculating the precision of a translation based on n-grams.
    An n-gram is a sequency of n consecutive tokens.
    Usually n is chosen between 1 and 4.
    A single n-gram precision p_n is calculated as following:
        p_n = m_{max} / w_t,
        where m_{max} is the maximum number of occurrences of the n_gram in the reference and
        w_t is the number of n-grams in the candidate.
    To calculate a Bleu score for a document over multiple n_gram precisions a geometric mean is calculated.
    To calculate a Bleu score over an entire corpus a geometric mean over
    all Bleu Scores of each document is calculated and a brevity penalty e^(1-r/c) is multiplied, where r is the
    length of the reference corpus and c is the length of the candidate.

    See https://en.wikipedia.org/wiki/BLEU#endnote_Madnani2011 and
    https://github.com/tensorflow/nmt/blob/master/nmt/scripts/bleu.py for more detail.

    Args:
        candidate (Iterable[List[str]]): a corpus of tokenized documents, this is usually machine generated
        reference (Iterable[List[str]]): a corpus of tokenized documents, this is usually made by humans

    Returns:
        float: value between 0 and 1

    Notes:
        If the candidate and the reference are not tokenized in the same way, then the bleu score
        is not useful. Make sure to use the same tokenizer or to use a non tokenization dependent metric like sacrebleu.
    """
    ref = _ReferencesWrapperCorpus(reference)
    _bleu = load_metric("bleu")
    _bleu.add_batch(predictions=candidate, references=ref)
    result = _bleu.compute()
    result["score"] = result["bleu"]
    out = {"bleu": result}
    return out


def gleu(candidate: Corpus, reference: Corpus) -> MetricReturn:
    """
    The BLEU score has some undesirable properties when used for single
    sentences, as it was designed to be a corpus measure. We therefore
    use a slightly different score for our RL experiments which we call
    the 'GLEU score'. For the GLEU score, we record all sub-sequences of
    1, 2, 3 or 4 tokens in output and target sequence (n-grams). We then
    compute a recall, which is the ratio of the number of matching n-grams
    to the number of total n-grams in the target (ground truth) sequence,
    and a precision, which is the ratio of the number of matching n-grams
    to the number of total n-grams in the generated output sequence. Then
    GLEU score is simply the minimum of recall and precision. This GLEU
    score's range is always between 0 (no matches) and 1 (all match) and
    it is symmetrical when switching output and target. According to
    our experiments, GLEU score correlates quite well with the BLEU
    metric on a corpus level but does not have its drawbacks for our per
    sentence reward objective.
    Computes corpus-level Google BLEU (GLEU) score of translated segments against one or more references.
    Instead of averaging the sentence level GLEU scores (i.e. macro-average precision), Wu et al. (2016) sum up the matching
    tokens and the max of hypothesis and reference tokens for each sentence, then compute using the aggregate values.

    Args:
        candidate (Iterable[List[str]]): a corpus of tokenized documents, this is usually machine generated
        reference (Iterable[List[str]): a corpus of tokenized documents, this is usually made by humans

    Returns:
        float: value between 0 and 1
    """

    ref = _ReferencesWrapperCorpus(reference)
    _google_bleu = load_metric("google_bleu")
    _google_bleu.add_batch(predictions=candidate, references=ref)
    res = _google_bleu.compute()
    res["score"] = res["google_bleu"]
    out = {"gleu": res}
    return out


def cosine_similarity(x: np.ndarray, y: np.ndarray) -> float:
    """
    This function calculates the cosine similarity between two vectors of the same R^n vector space.
    It ranged form -1 to 1. -1 means the vectors point in opposite directions. 1 means the same direction,
    And 0 means the vectors are orthogonal. For Term Frequency and TfIdf those values can not be negative
    as the matrix only contains positive values.
    Args:
        x (np.ndarray): 1d array of shape (n,) of a floating point type
        y (np.ndarray): 1d array of shape (n,) of a floating point type

    Returns:
        float: cosine similarity
    """
    return np.dot(x, y) / (np.linalg.norm(x) * np.linalg.norm(y))


def lsi_similarity(
    corpus_1: Corpus,
    corpus_2: Corpus,
    num_topics: int = 200,
    random_seed: int = None,
) -> MetricReturn:
    """
    Compute a Latent Semantic Indexing based similarity score between two documents.
    It is assumed the Corpora are leated to each other per document.
    The core idea is to build a BoW-matrix and calculate a LSI from this matrix. Then the two corpora are indvidually
    transformed in the Combined LSI Space. Finally each transformed document is compared using cosine similarity.
    If the texts are overall very similar a score close to 1 can be expected.
    If the texts are vastly didferent a score close to 0 can be expected.
    Args:
        corpus_1 (Iterable[List[str]]): A corpus in bag of words representation
        corpus_2 (Iterable[List[str]]): Another corpus in bag of words representation, related to corpus_1.
        e.g. corpus_1 is the reference text and corpus_2 is auto_completed by machine
        num_topics (int): max number of desired topics for LSI. For big corpora 200 to 300 often work fine.
        random_seed (int): seed to set numpys random number generator for reproducibility. If none no seed is set.

    Returns:
        float: 0 <= output <= 1
    """

    if random_seed is not None:
        np.random.seed(random_seed)

    # step 1 get the LSI
    # combine the corpora
    combined_corpus = AppendCorpus([corpus_1, corpus_2])
    # calculate Bag of Words and Term Frequency Inverse Document Frequency
    combined_bow_corpus = BoWCorpus(combined_corpus)
    # only provide the dictionary to be more efficient
    combined_tfidf_model = gensim.models.TfidfModel(
        dictionary=combined_bow_corpus.dictionary
    )
    combined_tfidf_corpus = combined_tfidf_model[combined_bow_corpus]
    # calculate LSI
    combined_lsi_model = gensim.models.LsiModel(
        combined_tfidf_corpus,
        num_topics=num_topics,
        id2word=combined_bow_corpus.dictionary,
    )
    # step 2 calculate the representations of the documents in the combines lsi space
    # to do that we need to transform the corpora into the bow represenation of the combined corpus
    corpus_1_bow = combined_bow_corpus.corpus_to_bow(corpus_1)
    corpus_2_bow = combined_bow_corpus.corpus_to_bow(corpus_2)

    # then we need to turn the bow into a tfidf
    corpus_1_tfidf = combined_tfidf_model[corpus_1_bow]
    corpus_2_tfidf = combined_tfidf_model[corpus_2_bow]

    # to finally feed it into the lsi
    corpus_1_lsi = combined_lsi_model[corpus_1_tfidf]
    corpus_2_lsi = combined_lsi_model[corpus_2_tfidf]

    # step 3 we calculate the cosine similarities to see how different the transformed documents are
    cosines = list(map(gensim.matutils.cossim, corpus_1_lsi, corpus_2_lsi))

    # step 4 return the mean as a score
    score = np.mean(cosines)
    out = dict()
    res = {"score": score, "score_per_document": score}
    out["lsi_similarity"] = res
    return out


# bleurt
# https://www.statmt.org/wmt19/metrics-task.html

# todo
# make function to gather metrics
# function that takes a bunch of pairs of reference and candidate corpora and calculates all the metrics from them
# and saves them to a dataframe

# make function to do plots from gathered metrics: barplot and paretofront
# debug lsi
