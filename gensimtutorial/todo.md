# Todo

## Software Structure

- a set of metrics functions that turn 2 corporas into a score
- a metrics collection object with the job to collect metrics and hold metainformation about them / about the process that created the data for the metrics
  - the metrics collection receives a list of metrics functions
- a plotting function that turns one / many metric(s) into a plot
- a server that get's metrics collections and is configured with some plotting function with the job to display the metrics in the metics collection
